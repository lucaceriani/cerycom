<?php
session_start();
/*
File: professori/index.php
*/
	require '../f.php';
	
	if (isset($_SESSION['user_p'])) {
		include ('principale.php');
	} elseif (isset($_POST['ok'])) {
	
		$user = clean($_POST['userid']);
		$password = md5($_POST['password']);
		
		if((check_professore ($user, $password)) == 1) {
			redir('index.php');
			echo 'Login effettuato con successo! Torna alla <a href="index.php">HOME</a>';
		} else {
			echo 'Erore durante la procedra di login! <a href="login.php">RITENTA</a>';
		}
		
	} else {
		redir('login.php');
	}
?>
