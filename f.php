<?php
/**

File: f.php

+---------------------------------+
|                                 |
|   PAGINA DI TUTTE LE FUNZIONI   |
|                                 |
+---------------------------------+

*/

require 'cfg.php';

function find_user ($id) {
	/**
	Trova il nome dell'alunno 
	*/
	require 'cfg.php';
	mysql_select_db($DB_ALU, $conn);
	$query = "SELECT * FROM indice WHERE ID = '".$id."'";
	$result = mysql_query($query, $conn);
	$row = mysql_fetch_assoc($result);
	$nome = $row['Nome'].' '.$row['Cognome'];
	return $nome;
}

function data_ita ($data) {
	/**
	Converte la data MySQL in data italiana 
	*/
	list ($y, $m, $d) = explode ("-", $data);
	return $d."/".$m."/".$y;
	}

function data_mysql ($data) {
	/**
	Converte la data Italiana in data MySQL
	*/
	list ($d, $m, $y) = explode ("/", $data);
	return $y."-".$m."-".$d;
}
	
function check_professore ($u, $p) {
	/**
	Login & check del professore 
	*/
	require 'cfg.php';
	mysql_select_db($DB_PRO, $conn);
	$query = "SELECT * FROM indice WHERE ID = '".$u."' AND Password = '".$p."';";
	$result = mysql_query($query, $conn);	
	$nr = mysql_num_rows($result);
	$row = mysql_fetch_row($result);
	if ($nr==1) {
		$_SESSION['user_p']=$u;
		$_SESSION['id_p']=$row[0];
		return 1;
	} else {
		return 0;
	}
}

function check_alunno ($u, $p) {
	/**
	Login & check dell'alunno 
	*/
	require 'cfg.php';
	mysql_select_db($DB_ALU, $conn);
	$query = "SELECT * FROM indice WHERE Username = '".$u."' AND Password = '".$p."';";
	$result = mysql_query($query, $conn);	
	$nr = mysql_num_rows($result);
	$row = mysql_fetch_row($result);
	if ($nr==1) {
		$_SESSION['user_a']=$u;
		$_SESSION['id_a']=$row[0];
		return 1;
	} else {
		return 0;
	}
}

function pass() {
	/**
	Creazione password per gli alunni
	*/
	$mypass="";
	$lung_pass = 5;
	for ($x=1; $x<=$lung_pass; $x++)
	{
	/*if ($x > $lung_pass - 2){
		$mypass = $mypass.rand(0,9);
	}else{
		$mypass = $mypass.chr(rand(97,122));
	}*/
	$mypass = $mypass.rand(0,9);
	}
	
	$mypass = "ciao"; //Utilizzato in fase di debug
	
return $mypass;
}

function user($id) {
	/**
	Creazione username per gli alunni tenendo conto dell'ID
	per non generare due username uguali
	*/
	$l = 5;
	$r = '';
	/**
	$r = $r.chr(rand(97,122));
	$r = $r.chr(rand(97,122));
	$r = $r.chr(rand(97,122));
	*/
	$r = $r.rand(0,9);
	$r = $r.rand(0,9);
	$r = $r.rand(0,9);
	$r = $r.(55 + $id);
	return $r;
}

function redir($dest) {
	header("location: ".$dest);
}

function admin_logged() {
	/* Controllo se l'admin è loggato */
	if (@$_SESSION['log'] == 'admin') {
		return 1;
	} else {
		return 0;
	}
}

function alunno_logged() {
	/* Controllo se lo studente è loggato */
	if (isset($_SESSION['user_a'])) {
		return 1;
	} else {
		return 0;
	}
}
function prof_logged() {
	/* Controllo se lo studente è loggato */
	if (isset($_SESSION['user_p'])) {
		return 1;
	} else {
		return 0;
	}
}

function clean($str, $mode='cap') {
	require 'cfg.php';
	$res = mysql_real_escape_string($str);
	if ($mode == 'cap') {
		return ucwords(strtolower($res));
	} elseif ($mode = 'n') {
		return $res;
	}
}

function get_class_name($id) {
	require 'cfg.php';
	mysql_select_db($DB_CLA, $conn);
	$res = mysql_query("SELECT Classe FROM indice WHERE ID='".$id."';");
	$row = mysql_fetch_assoc($res);
	return $row['Classe'];
	
}

function get_mat_name($id) {
	require 'cfg.php';
	mysql_select_db($DB_OPZ, $conn);
	$res = mysql_query("SELECT * FROM materie WHERE ID='".$id."';");
	$row = mysql_fetch_assoc($res);
	return $row['Materia'];
	
}

function print_assenze($id, $res, $tipo) {
	
	if (mysql_num_rows($res) < 1) {
		echo '<td class="ab"><center>'.mysql_num_rows($res).'</center></td>';
	} else {
		echo '<td class="ab"><a href="dettassenze.php?id='.$id.'&t='.$tipo.'"><div><center>'.mysql_num_rows($res).'</center></div></a></td>'; /* Link per i dettagli  */
	}
}

function check($x) {
	require 'cfg.php';
	if (!isset($_SESSION['user_'.$x])) { die($ERROR_403);} else {return $_SESSION['id_'.$x];}
}

?>
