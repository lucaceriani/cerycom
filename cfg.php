<?php
/**
File: cfg.php

>>> ATTENZIONE: Non modificare queste informazioni se non sai cosa stai fancendo! <<<

*/

$DB_HOST = "localhost";
$DB_USER = "root";
$DB_PASS = "";
$conn = mysql_connect($DB_HOST, $DB_USER, $DB_PASS);

if (!$conn) {
	die(mysql_error());
}

/**
Password per amministrare il sito.
*/

$PASS_ADMIN = "admin";

/**
Nomi database
*/

$DB_ALU = "alunni";
$DB_PRO = "professori";
$DB_CLA = "classi";
$DB_OPZ = "opzioni";


/**
Messaggi di sistema
*/
$ERROR_403 = "<h1>Errore 403 - Accesso negato!</h1>";
$ERROR_INC = "<h1>Errore Inconguenza</h1>";

/**
Varie
*/
$TODAY = date("Y-m-d");


?>
